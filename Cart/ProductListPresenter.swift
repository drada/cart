//
//  ProductListPresenter.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation


protocol ProductListView {
	func showProducts(products: [Product])
	func showTotalQuantity(quantity: Int)
}


class ProductListPresenter {
	
	var view: ProductListView!
	var productsInteractor = ProductsInteractor()
	var cartInteractor = CartInteractor()
	
	private var products: [Product]?
	
	func viewDidLoad() {
		self.products = self.productsInteractor.productList()
		
		self.view.showProducts(self.products!)
	}
	
	func userDidTapProduct(product: Product) {
		self.cartInteractor.addProduct(product)
		let quantity = self.cartInteractor.totalQuantity()
		self.view.showTotalQuantity(quantity)
	}
	
}
