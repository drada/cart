//
//  ViewController.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import UIKit

class ProductListVC: UIViewController, ProductListView, UITableViewDataSource, UITableViewDelegate {
	
	let presenter = ProductListPresenter()
	
	private var products: [Product]?
	
	@IBOutlet weak var itemCart: UIBarButtonItem!

	
	// MARK: - View Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.presenter.view = self
		self.presenter.viewDidLoad()
	}
	
	
	// MARK: - TableView DataSource
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let number = self.products?.count else {
			return 0
		}
		
		return number
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("CellProduct") as! ProductCell
		
		cell.productName.text = self.products![indexPath.row].productName
		cell.productPrice.text = self.products![indexPath.row].productPrice + "€"
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let product = self.products![indexPath.row]
		
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		
		self.presenter.userDidTapProduct(product)
	}
	
	
	// MARK: - Presenter
	
	func showProducts(products: [Product]) {
		self.products = products
	}
	
	func showTotalQuantity(quantity: Int) {
		self.itemCart.title = "\(quantity) Products"
	}

}

